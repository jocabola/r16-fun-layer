# TOOLS
BIN = node_modules/.bin
BROWSERIFY = ${BIN}/browserify
#SASS = ${BIN}/node-sass
COFFEE = ${BIN}/coffee
SERVER = ${BIN}/live-server
COMPILER = utils/compiler/compile.js
OBJ = utils/obj2threejs-batch-converter/convert.js

# PATHS
JS=public/assets/js

# CONFIG
SERVER_PORT = 8989
FLAGS="--debug"

LIBS_IN = src/libs-main.js
LIBS_OUT = ${JS}/libs.js

MAIN_IN = src/main.coffee
MAIN_OUT = ${JS}/main.js
MAIN_COMPRESSED = deploy/js/main.js

# LIBRARY_IN = src/apps/App.coffee
# LIBRARY_OUT = build/

SHADER_IN = src/glsl
SHADER_OUT = public/assets/glsl

default: build

clean:
	-rm ${MAIN_OUT}

clean-all: clean
	-rm -R utils/compiler/node_modules
	-rm -R node_modules
	-rm -R deploy

install:
	npm install
	npm install --prefix utils/compiler

libs:
	mkdir -p ${JS}
	${BROWSERIFY} ${LIBS_IN} ${FLAGS} -o ${LIBS_OUT}

build:
	mkdir -p ${JS}
	${BROWSERIFY} -t coffeeify -t glslify ${MAIN_IN} ${FLAGS} -o ${MAIN_OUT}
	
deploy: libs build
	node ${COMPILER} ${COMPILER_FLAGS}

symbols:
	#node ${OBJ}
	python utils/fbx2threejs/convert_to_threejs.py symbols-in/scene.fbx public/assets/symbols/scene.js

# library:
# 	mkdir -p ${LIBRARY_OUT}
# 	${COFFEE} -c -o ${LIBRARY_OUT} ${LIBRARY_IN}

server:
	${SERVER} --port=${SERVER_PORT} public

server-deploy:
	${SERVER} --port=${SERVER_PORT} deploy