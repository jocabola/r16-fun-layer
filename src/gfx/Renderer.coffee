class Renderer
	constructor: ( container ) ->
		w = window.innerWidth
		h = window.innerHeight
		@renderer = new THREE.WebGLRenderer { antialias: true }
		@renderer.setSize w, h
		container.appendChild @renderer.domElement

		@camera = new THREE.OrthographicCamera -w/2, w/2, h/2, -h/2, 1, 10000

		@scene = new THREE.Scene()
		@scene.add @camera

		if window.addEventListener
			window.addEventListener 'resize', ( e ) =>
				@resize()
			, false
		else if window.attachEvent
			window.attachEvent 'resize', ( e ) =>
				@resize()
			, false

	render: ( grid ) ->
		@renderer.autoClear = false
		@renderer.clear()
		if grid.active
			@renderer.render grid.scene, @camera
			@renderer.clearDepth()
		@renderer.render @scene, @camera

	resize: () ->
		w = window.innerWidth
		h = window.innerHeight
		@renderer.setSize w, h
		@renderer.setSize w, h
		@camera.left = -w/2
		@camera.right = w/2
		@camera.top = h/2
		@camera.bottom = -h/2
		@camera.updateProjectionMatrix()

module.exports = Renderer;