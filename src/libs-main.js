// Expose all libraries that you are going to use.
dat = require( 'dat-gui' );
Stats = require( 'stats-js' );
THREE = require( 'three' );
gsap = require( 'gsap' );
Detector = require( './lib/Detector.js' );
require( './lib/SceneLoader.js' );
require( './lib/ColladaLoader.js' );