class Grid
	constructor: ( rect ) ->
		@defineGrid rect

		# for drawing grid
		@generateGeometry()

		@material = new THREE.PointsMaterial
			size: @cellSize * .98
			sizeAttenuation: false
			#color: 0x333333
			vertexColors: THREE.VertexColors

		@mesh = new THREE.Points @geometry, @material
		@mesh.position.set 0, 0, -1
		#@mesh.visible = false
		@active = false
		@scene = new THREE.Scene()
		@scene.add @mesh

		@cells = []
		@cells.push false for k in [0...@nCells]

	generateGeometry: ->
		@geometry = new THREE.BufferGeometry()
		nv = @nCells
		center = new THREE.Vector2 @nCols * @cellSize / 2, @nRows * @cellSize / 2
		p = new Float32Array nv * 3
		c = new Float32Array nv * 3
		k = 0
		hs = @cellSize/2

		@color = new THREE.Color( 0x222222 )

		for i in [0...@nRows]
			for j in [0...@nCols]
				pi = k * 3
				x = -center.x + j * @cellSize + hs
				y = -center.y + i * @cellSize + hs
				p[ pi + 0 ] = x
				p[ pi + 1 ] = -y
				p[ pi + 2 ] = 0

				c[ pi + 0 ] = @color.r
				c[ pi + 1 ] = @color.g
				c[ pi + 2 ] = @color.b

				k++

		@geometry.addAttribute "position", new THREE.BufferAttribute( p, 3 )
		@geometry.addAttribute "color", new THREE.BufferAttribute( c, 3 )

	defineGrid: ( rect ) ->
		s = 10
		@cellSize = 100
		@nCols = Math.floor( rect.x / @cellSize )
		@nRows = Math.floor( rect.y / @cellSize )
		while @nCols < s or @nRows < s
			@cellSize *= .99
			@nCols = Math.floor( rect.x / @cellSize )
			@nRows = Math.floor( rect.y / @cellSize )
		@dx = rect.x - @nCols * @cellSize
		@dy = rect.y - @nRows * @cellSize
		@nCells = @nCols * @nRows
		console.log "Grid Settings: { cellSize: #{@cellSize}, nRows: #{@nRows}, nCols: #{@nCols} }"

	setSize: ( w, h ) ->
		@geometry.dispose()
		@geometry = null
		@defineGrid new THREE.Vector2( w, h )
		@generateGeometry()
		@mesh.geometry = @geometry
		@material.size = @cellSize * .98
		@cells = []
		@cells.push false for k in [0...@nCells]

	selectCells: ->
		ci = Math.round( Math.random() * ( @nCells - 1 ) )
		cpos = ci % @nCols
		rpos = Math.floor( ci / @nCols )
		#console.log "Select cell #{ci} on (#{cpos}, #{rpos})"

		maxC = Math.min( 3, @nCols - cpos )
		maxR = Math.min( 3, @nRows - rpos )
		m = Math.min maxC, maxR
		#console.log maxC, maxR, m
		s = 1 + Math.round( Math.random() * ( m - 1 ) )
		#console.log "Generating creature of #{s}x#{s} cells"

		r = [ ci ]

		@markCell ci
		# mark adjacent cells
		for k in [1...s] by 1
			@markCell ci + k # adjacent column
			r.push ci + k
			for k2 in [ 0...s ]
				@markCell ci + k*@nCols + k2 # adjacent row / col
				r.push ci + k*@nCols + k2

		return r

	markCell: ( ci ) ->
		cols = @geometry.attributes.color
		cindex = ci * 3
		cols.array[ cindex + 0 ] = .8
		cols.array[ cindex + 1 ] = .2
		cols.array[ cindex + 2 ] = .2
		cols.needsUpdate = true

		@cells[ci] = true

	unmarkCell: ( ci ) ->
		cols = @geometry.attributes.color
		cindex = ci * 3
		cols.array[ cindex + 0 ] = @color.r
		cols.array[ cindex + 1 ] = @color.g
		cols.array[ cindex + 2 ] = @color.b
		cols.needsUpdate = true

		@cells[ci] = false

	unmarkCells: ( ca ) ->
		@unmarkCell ci for ci in ca

	markCells: ( ca ) ->
		@markCell ci for ci in ca

	getCellPosition: ( ci ) ->
		#console.log "Fetching position of cell #{ci}"
		cpos = ci % @nCols
		rpos = Math.floor( ci / @nCols )
		center = new THREE.Vector2 @nCols * @cellSize / 2, @nRows * @cellSize / 2
		x = -center.x + cpos * @cellSize
		y = -center.y + rpos * @cellSize

		return new THREE.Vector2 x, -y

module.exports = Grid