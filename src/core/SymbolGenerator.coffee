glslify = require 'glslify'
Grid = require './Grid.coffee'

class SymbolGenerator
	constructor: ( url="assets/symbols/scene.js" ) ->
		@grid = new Grid new THREE.Vector2( window.innerWidth, window.innerHeight )
		@rotations = [ 0, Math.PI/4, -Math.PI/4, Math.PI/2, -Math.PI/2 ]
		@colors1 = [
			new THREE.Color( 0.372, 0.725, 0.313 )
			new THREE.Color( 0.372, 0.725, 0.313 )
			new THREE.Color( 0.921, 0.137, 0.156 )
			new THREE.Color( 0.392, 0.176, 0.549 )
			new THREE.Color( 0.988, 0.83, 0.889 )
			new THREE.Color( 0.921, 0.137, 0.156 )
		]
		@colors2 = [
			new THREE.Color( 0.392, 0.784, 0.901 )
			new THREE.Color( 1, 1, 0.156 )
			new THREE.Color( 1, 1, 0.156 )
			new THREE.Color( 0.392, 0.784, 0.901 )
			new THREE.Color( 0.392, 0.176, 0.549 )
			new THREE.Color( 0.988, 0.83, 0.889 )
		]

		## deactivate console.warn
		console.warn = ->
			return

		@geometries = []
		@maxElements = 50

		@container = new THREE.Object3D()
		@container.position.set 0, 0, -5
		@elements = []

		@frameCount = 0
		@gFreq = 60

		@material = new THREE.ShaderMaterial
			vertexShader: glslify '../glsl/symbol.vert'
			fragmentShader: glslify '../glsl/symbol.frag'
			side: THREE.DoubleSide
			transparent: true
			uniforms:
				time:
					type: 'f'
					value: 0
				progress:
					type: 'f'
					value: 0
				resolution:
					type: 'v2'
					value: new THREE.Vector2 window.innerWidth, window.innerHeight
				color1:
					type: 'c'
					value: null
				color2:
					type: 'c'
					value: null
				mode:
					type: 'i'
					value: 1#Math.round( Math.random() )
				rotation:
					type: 'f'
					value: 0
				wobbling:
					type: 'f'
					value: 1
				size:
					type: 'f'
					value: 50


		addObject = ( o ) =>
			console.log "Adding Symbol #{o.name}..."

			geo = [ null, null, null ]

			for c in o.children
				continue if c == undefined
				###console.log "Found #{c.name}..."
				console.log c###
				c.geometry.computeBoundingSphere()
				c.geometry.computeBoundingBox()
				c.geometry.computeVertexNormals()
				c.geometry.computeFaceNormals()
				k = -1
				n = c.name.toLowerCase() 
				if n.indexOf( "1x" ) > -1 # 100 x 100
					k = 0
				else if n.indexOf( "2x" ) > -1 # 200 x 200
					k = 1
				else if n.indexOf( "3x" ) > -1 # 300 x 300
					k = 2

				if k > -1
					geo[k] = c.geometry.clone()
					#geo[k] = new THREE.PlaneBufferGeometry( (k+1) * 100, (k+1) * 100, 32, 32 )
					###geo[k] = new THREE.BufferGeometry()
					geo[k].fromGeometry c.geometry###

			@geometries.push geo

		window.container = @container
		window.geometries = @geometries

		onSceneLoaded = ( root, materials ) ->
			#console.log root.scene.children
			addObject o for o in root.scene.children

		loader = new THREE.SceneLoader()
		loader.load url, onSceneLoaded

	generate: ->
		return if @geometries.length == 0
		return if @container.children.length >= @maxElements
		###return if @doit
		@doit = true###
		
		g = @grid.selectCells()
		s = Math.sqrt g.length # gsize

		# check if intersects

		for e in @elements
			for gi in e.grid
				if g.indexOf( gi ) > -1
					#intersects!
					@grid.unmarkCells e.grid
					@killSoftly e.mesh
					@grid.markCells g
					break


		### attributes ###
		scl = @grid.cellSize / 100
		rot = @rotations[ Math.round( Math.random() * ( @rotations.length - 1 ) ) ]
		ci = Math.round( Math.random() * ( @colors1.length - 1 ) )
		inverseColors = Math.random() < .5

		# create copy of same shader program but with custom uniforms
		mat = @material.clone()
		mat.uniforms.color1.value = if !inverseColors then @colors1[ci] else @colors2[ci]
		mat.uniforms.color2.value = if !inverseColors then @colors2[ci] else @colors1[ci]
		mat.uniforms.rotation.value = -rot
		mat.uniforms.progress.value = 0
		mat.uniforms.mode.value = Math.round( Math.random() )
		mat.uniforms.size.value = 50 * s
		#mat.wireframe = true
		#mat.wireframeLinewidth = 10

		###mat = new THREE.MeshBasicMaterial 
			color: 0xffffff * Math.random()
			side: THREE.DoubleSide###

		geo = @geometries[ Math.round( Math.random() * ( @geometries.length - 1 ) ) ][s-1]

		#console.log geo

		mesh = new THREE.Mesh geo, mat
		@container.add mesh
		mesh.scale.set scl, scl, scl
		mesh.rotation.z = rot
		pos = @grid.getCellPosition g[0]
		mesh.position.set pos.x + 50 * scl * s, pos.y - 50 * scl * s, 0#if Math.random() < .5 then 4 else 0
		###mat.wireframeLinewidth = 20
		mat.wireframe = true###

		@elements.push 
			mesh: mesh
			grid: g

		kill = =>
			@removeMesh mesh

		die = =>
			@grid.unmarkCells g
			TweenLite.to mat.uniforms.progress, 6 + 3 * Math.random(), { value: 2, onComplete: kill }

		onComplete = =>
			t = Math.random() * 2000
			window.setTimeout die, t

		TweenLite.to mat.uniforms.progress, 4 + Math.random() * 3, { value: 1, onComplete: onComplete }

	killSoftly: ( mesh ) ->
		mat = mesh.material
		TweenLite.killTweensOf mat.uniforms.progress

		kill = =>
			@removeMesh mesh, false

		TweenLite.to mat.uniforms.progress, 5 + 2 * Math.random(), { value: 2, onComplete: kill }

	removeMesh: ( mesh ) ->
		for k,e of @elements
			if e.mesh == mesh
				#gotcha
				@container.remove mesh
				@elements.splice k, 1
				return

	update: ( time ) ->
		@generate() if @frameCount % @gFreq == 0
		@frameCount++

		mesh.material.uniforms.time.value = time for mesh in @container.children

	setSize: ( w, h ) ->
		for e in @elements
			@killSoftly e.mesh
		@elements.splice 0, @elements.length
		@grid.setSize w, h

module.exports = SymbolGenerator