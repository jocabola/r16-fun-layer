Renderer = require '../gfx/Renderer.coffee'
SymbolGenerator = require '../core/SymbolGenerator.coffee'
glslify = require 'glslify'

class App
	constructor: ( @container ) ->
		@renderer = new Renderer @container
		@time = 0
		@oTime = Date.now()

		@generator = new SymbolGenerator "assets/symbols/scene.js"
		#@renderer.scene.add @generator.grid.mesh
		@renderer.scene.add @generator.container

		###onLoaded = ( geo, materials ) =>
			geo.computeVertexNormals()

			bgeo = new THREE.BufferGeometry()
			bgeo.fromGeometry geo
			bgeo.computeBoundingBox()
			bgeo.computeBoundingSphere()

			mat = @generator.material.clone()
			mat.uniforms.color1.value = new THREE.Color( 1, 1, 1 )
			mat.uniforms.color2.value = new THREE.Color( 1, 1, 1 )
			mat.uniforms.mode.value = 1
			mat.uniforms.progress.value = 0
			mat.uniforms.wobbling.value = 0.2
			mat.uniforms.rotation.value = Math.PI/4

			@logo = new THREE.Mesh bgeo, mat
			scl = @generator.grid.cellSize * 4 / 100
			@logo.scale.set scl, scl, scl
			@logo.rotation.z = -Math.PI/4
			@logo.position.set scl * 2.5, 0, -2
			@renderer.scene.add @logo

			TweenLite.to mat.uniforms.progress, 6, { value: 1 }

			# setup mouse controls
			
			onMouseMove = ( e ) =>
				mx =  ( e.clientX / window.innerWidth ) * 2 - 1
				my = -( e.clientY / window.innerHeight ) * 2 + 1
				
				m = new THREE.Vector2 mx * window.innerWidth, my * window.innerHeight

				s = @logo.scale.x * 100

				intersect = m.x > -s * .85 and m.x < s * .85 and m.y > -s and m.y < s

				if intersect and !@intersected
					col1 = @generator.colors1
					col2 = @generator.colors2
					ci = Math.round( Math.random() * ( col1.length-1 ) )
					@logo.material.uniforms.mode.value = Math.round Math.random()
					#TweenLite.killTweensOf( @logo.material.uniforms.progress )
					TweenLite.to @logo.material.uniforms.wobbling, 1, { value: 1 }
					#TweenLite.to @logo.material.uniforms.progress, 1, { value: 1.5 }
					TweenLite.to @logo.material.uniforms.color1.value, 1, { r: col1[ci].r, g: col1[ci].g, b: col1[ci].b }
					TweenLite.to @logo.material.uniforms.color2.value, 1, { r: col2[ci].r, g: col2[ci].g, b: col2[ci].b }
					@intersected = true
				else if @intersected and !intersect
					#TweenLite.killTweensOf( @logo.material.uniforms.progress )
					#@logo.material.uniforms.progress.value = 0
					#TweenLite.to @logo.material.uniforms.progress, 6, { value: 1 }
					TweenLite.to @logo.material.uniforms.wobbling, 1, { value: .2 }
					TweenLite.to @logo.material.uniforms.color1.value, 1, { r: 1, g: 1, b: 1 }
					TweenLite.to @logo.material.uniforms.color2.value, 1, { r: 1, g: 1, b: 1 }
					@intersected = false

			if !Main.isMobile()
				window.addEventListener "mousemove", onMouseMove, false

		loader = new THREE.JSONLoader()
		loader.load "assets/symbols/logo-icon.js", onLoaded###

		window.renderer = @renderer

		if window.addEventListener
			window.addEventListener 'resize', ( e ) =>
				@resize()
			, false
		else if window.attachEvent
			window.attachEvent 'resize', ( e ) =>
				@resize()
			, false

		#@gui = new dat.GUI()

		# debug options
		onKeyDown = ( e ) =>
			#console.log e.keyCode
			switch e.keyCode
				when 71 # g
					@generator.grid.active = !@generator.grid.active

		window.addEventListener "keydown", onKeyDown, false

	update: () ->
		time = ( Date.now() - @oTime ) * .001
		dt = time - @time
		@time = time

		@logo.material.uniforms.time.value = time if @logo

		@generator.update time
	
	render: () ->
		@renderer.render @generator.grid

	resize: () ->
		@generator.setSize window.innerWidth, window.innerHeight
		if @logo
			scl = @generator.grid.cellSize * 4 / 100
			@logo.scale.set scl, scl, scl
			@logo.position.x = scl * 2.5

module.exports = App

window.Res16FunLayer = App