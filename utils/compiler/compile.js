var compressor = require('node-minify');
var fs = require('fs');
//var execSync = require('exec-sync');
var execSync = require('child_process').execSync;
var mkdirp = require( "mkdirp" );

var flag = process.argv[2];

// ----- CONFIG AREA ---------------------------------------

// ASSETS
var ASSETS_VERSION = 3;
var CDN_URL = "./";

// SRC FILE TO SCAN
var src_path = 'public/';
var src = src_path + 'index.html';

// DEPLOY ROOT
var deploy_path = 'deploy/';

// FILES TO COPY
var deploy_files = [];
deploy_files.push( 'assets/' );

// ---------- COMPILER -------------------------------------

console.log( 'Generating version ' + ASSETS_VERSION + '...' );

console.log( 'Scanning HTML file...' );

var string = fs.readFileSync( src, { encoding: 'utf-8' } );

console.log( 'Copying Files to Deploy...' );

mkdirp.sync( deploy_path );

if ( flag && flag == "--clean" ) {
    console.log( "Cleaning deploy path..." );
    execSync('rm -rf ' + deploy_path + '*' );
}

for ( k=0; k<deploy_files.length; k++ ) {
    var df = deploy_files[k];
    console.log( 'Copying ' + df );
    execSync( "rsync -av --delete --exclude=.git --exclude=.gitignore --exclude=.DS_Store --exclude=libs.js --exclude=main.js " + src_path + df + " " + deploy_path + df );
}

console.log( 'Generating index.html...' );

var ifile = fs.openSync( deploy_path + 'index.html', 'w' );
string = string.replace( '<script type="text/javascript" src="assets/js/libs.js"></script>', '' );
fs.writeSync( ifile, string.replace( /\<\?=\$assets_version\?\>/gm, ASSETS_VERSION ) );
fs.close( ifile );

console.log( "Minifying JS files [UglifyJS] ..." );

new compressor.minify({
    type: 'uglifyjs',
    fileIn: [ src_path + 'assets/js/libs.js' ],
    fileOut: deploy_path + 'assets/js/libs.js',
    callback: function(err, min){
        if ( err != null )
            console.log(err);
        else {
            new compressor.minify({
                type: 'uglifyjs',
                fileIn: [ src_path + 'assets/js/main.js' ],
                fileOut: deploy_path + 'assets/js/main.js',
                callback: function(err, min){
                    if ( err != null )
                        console.log(err);
                    else {
                        // combine both files
                        var libsjs = fs.readFileSync( deploy_path + 'assets/js/libs.js', { encoding: 'utf-8' } );
                        var js = fs.readFileSync( deploy_path + 'assets/js/main.js', { encoding: 'utf-8' } );
                        js = 'var aURL="' + CDN_URL + '";' + js;
                        var jsfile = fs.openSync( deploy_path + 'assets/js/main.js', 'w' );
                        fs.writeSync( jsfile, libsjs + js );
                        fs.close( jsfile );
                        // remove libs js from deploy
                        fs.unlinkSync( deploy_path + 'assets/js/libs.js' );
                    }
                }
            });
        }
    }
});

console.log( "Minifying CSS files [YUI] ..." );

new compressor.minify({
    type: 'yui-css',
    fileIn: src_path + 'assets/css/styles.css',
    fileOut: deploy_path + 'assets/css/styles.css',
    callback: function(err, min){
        if ( err != null )
            console.log(err);
    }
});