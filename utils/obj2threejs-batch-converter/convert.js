var fs = require('fs');
var execSync = require('child_process').execSync;

var files = fs.readdirSync( "obj" );

var output = 'public/assets/symbols';

execSync( "mkdir -p " + output );

for ( var i=0; i<files.length; i++ ) {
	var f = files[i];
	if ( f.indexOf( ".obj" ) > -1 ) {
		var fout = output + "/" + f.replace( ".obj", ".js" );
		console.log( "Encoding", f, "..." );
		execSync( "python " + __dirname + "/convert_obj_three.py -i " + "obj/" + f + " -o " + fout );
		stats = fs.statSync( fout );
 		console.log( "Generated", fout, "/", stats["size"]/1024, "KB" );
	}
}